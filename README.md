
## Octave radio interferometer

I wrote this up years ago for a course and was recently asked for it.
The layout is reminiscent of the now-venerable-but-new-at-the-time [Virtual Radio Interferometer](https://www.adass.org/adass/proceedings/adass97/mckayn.html) from which I learned when I was an undergraduate&mdash;it was written in 1996 in this new language called "Java".

I don't have antenna stations or earth rotation or anything; it is really simply a demo in Fourier optics masks.

The octave script is quite short and pretty self explanatory.
If you clone the whole repo, there are other test images and masks; point the symbolic links to whatever you like!
It makes outputs that look like this:

![example1.png](example1.png)

and this:
![example2.png](example2.png)

