pkg load image

##
##  the two input files
##   change symbolic links and re-run
##
uv  = imread('tracks.png');
sky = imread('picture.png');

##  the files' 2D FFTs
SF = fft2(sky);
LM = fft2(uv);

##  remap the quadrants of the FFT outputs
##   so that DC is at the center and the
##   high half of the band are negative freqs
##  for simplicity, the two input images
##   must have the same dims
m = length(uv)/2;

dirty_beam = [...
 LM(m+1:2*m,m+1:2*m), LM(m+1:2*m,1:m);...
 LM(1:m,m+1:2*m),     LM(1:m,1:m)...
 ];
 
skyfft = [...
 SF(m+1:2*m,m+1:2*m), SF(m+1:2*m,1:m);...
 SF(1:m,m+1:2*m),     SF(1:m,1:m)...
 ];

##  make a float array of the png image
##  for convolution via multiplication
##  and FFT the convolved matrix back
##  to image space
uv_floats   = im2double(uv);
convo       = uv_floats .* skyfft;
dirty_image = ifft2( convo );

##
##  plotting
##
figure(1)

subplot(2,2,1)
imshow( sky, [] )
title( 'raw image' )

subplot(2,2,4)
image( abs(dirty_beam) )
axis square
title( 'dirty beam' )

subplot(2,2,3)
imshow( abs(dirty_image), [] )
title( 'dirty image' )

subplot(2,2,2)
imshow( uv_floats, [] )
title( 'uv coverage' )
